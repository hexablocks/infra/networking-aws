variable "aws_region" {}

variable "scope_name" { default = "" }
variable "tags" { default = {} }

variable "vpc_cidr_prefix" {}
variable "enable_ssm_vpce" { default = false }

variable "enable_nat_gateway" { default = false }
variable "one_nat_gateway_per_az" { default = false }
